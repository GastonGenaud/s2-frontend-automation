import { testController } from '../support/world'
import { select } from '../support/utils'
import { ClientFunction } from 'testcafe';

const dotenv = require('dotenv');

export class Home {
  constructor () {
    dotenv.config();
    this.url = process.env.URL_HOST + `login`
    this.urlHome = process.env.URL_HOST + `apps/home`
  }

  //HOME SELECTORS

  logoutButton() {
    return select('body > div.MuiPopover-root-131 > div.MuiPaper-root-25.MuiPopover-paper-132.py-8.MuiPaper-elevation8-36.MuiPaper-rounded-26 > li')
  }

  userSettings() {
    return select('#fuse-toolbar > div > div:nth-child(2) > button > span.MuiButton-label-58')
  }
  homeTitle () {
    return select ('.text-24').innerText
  }

  wellcomeHome () {
    return select ('.mb-16').exists
  }

  async navigate () {
    await testController.navigateTo(this.url)
  }

  async navigateHome () {
    dotenv.config();
    this.urlHome = process.env.URL_HOST + `apps/home`;
    console.log(this.urlHome);
    const getLocation = ClientFunction(() => document.location.href).with({ boundTestRun: testController });
    await testController.expect(getLocation()).contains(this.urlHome);
  }
  async navigateHomeAndExit () {
    dotenv.config();
    this.urlHome = process.env.URL_HOST + `apps/home`;
    console.log(this.urlHome);
    const getLocation = ClientFunction(() => document.location.href).with({ boundTestRun: testController });
    await testController
    .expect(getLocation()).contains(this.urlHome)
    .click(this.userSettings())
    .click(this.logoutButton())
    .expect(getLocation()).contains(this.url);
  }

  async homeValidate () {
    const homeTitle = this.homeTitle();
    const wellcomeHome = this.wellcomeHome();

    await testController
      .expect(wellcomeHome).ok('El texto de bienvenido a searchMas se visualiza', { allowUnawaitedPromise: false })
      .expect(homeTitle).eql("Inicio")
  }
}
